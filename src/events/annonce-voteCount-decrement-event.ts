import { Subjects } from './subjects';
import { VotingStatus } from './types/voting-status';

export interface AnnonceVoteCountDecrementEvent {
  subject: Subjects.AnnonceVoteCountDecrement;
  data: {
    id: string;
    version: number;
    userId: string;
    status: VotingStatus;
    annonce: {
      id: string;
      voteCount: number;
      voters: string[];
    };
  };
}
