export enum Subjects {
  AnnonceCreated = 'annonce:created',
  AnnonceUpdated = 'annonce:updated',

  AnnonceVoteCountIncrement = 'voteCount:increment',
  AnnonceVoteCountDecrement = 'voteCount:decrement',
  AnnonceVoteCountUpdated = 'voteCount:updated',

  CommentCreated = 'comment:created',
  CommentUpdated = 'comment:updated',

  MessageCreated = 'message:created',
  MessageUpdated = 'message:updated',
  MessageDeleted = 'message:deleted',

  OrderCreated = 'order:created',
  OrderCancelled = 'order:cancelled',

  FavoriteCreated = 'favorite:created',
  FavoriteUpdated = 'favorite:updated',

  ExpirationComplete = 'expiration:complete',

  PaymentCreated = 'payment:created',
}
