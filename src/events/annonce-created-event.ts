import { Subjects } from './subjects';

export interface AnnonceCreatedEvent {
  subject: Subjects.AnnonceCreated;
  data: {
    id: string;
    version: number;
    title: string;
    description: string;
    image: string;
    adress: string;
    phone: string;
    price: number;
    shippingFee: number;
    voteCount: number;
    userId: string;
  };
}
