import { Subjects } from './subjects';

export interface CommentCreatedEvent {
  subject: Subjects.CommentCreated;
  data: {
    id: string;
    text: string;
    userId: string;
    annonceId: string;
    createdAt: string;
  };
}
