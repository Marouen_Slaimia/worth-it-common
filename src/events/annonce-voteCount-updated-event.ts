import { Subjects } from './subjects';
import { VotingStatus } from './types/voting-status';

export interface AnnonceVoteCountUpdateEvent {
  subject: Subjects.AnnonceVoteCountUpdated;
  data: {
    id: string;
    version: number;
    userId: string;
    status: VotingStatus;
    annonce: {
      id: string;
      voteCount: number;
    };
  };
}
