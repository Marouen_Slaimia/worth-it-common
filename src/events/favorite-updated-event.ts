import { Subjects } from './subjects';

interface AnnonceDoc {
  title: string;
  version: number;
}

export interface FavoriteUpdatedEvent {
  subject: Subjects.FavoriteUpdated;
  data: {
    id: string;
    version: number;
    userId: string;
    annonces?: Array<AnnonceDoc>;
  };
}
