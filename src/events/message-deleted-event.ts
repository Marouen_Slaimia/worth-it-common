import { Subjects } from './subjects';

export interface MessageDeletedEvent {
  subject: Subjects.MessageDeleted;
  data: {
    id: string;
    version: number;
    annonce: {
      id: string;
    };
  };
}
