import { Subjects } from './subjects';

export interface FavoriteCreatedEvent {
  subject: Subjects.FavoriteCreated;
  data: {
    id: string;
    version: number;
    userId: string;
  };
}
