export enum VotingStatus {
  // The user has not express his attitude opposite to the advert
  AwaitingVote = 'awaiting:vote',

  LikeVote = 'like:vote',

  DislikeVote = 'dislike:vote',
}
