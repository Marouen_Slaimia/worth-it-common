export enum MessageStatus {
  // The user has not express his attitude opposite to the advert
  Created = 'created',

  Deleted = 'deleted',

  Updated = 'updated',
}
