import { Subjects } from './subjects';

export interface CommentUpdatedEvent {
  subject: Subjects.CommentUpdated;
  data: {
    id: string;
    text: string;
    userId: string;
    annonceId: string;
    createdAt: string;
    version: number;
  };
}
