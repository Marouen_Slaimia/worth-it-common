import { Subjects } from './subjects';

export interface AnnonceUpdatedEvent {
  subject: Subjects.AnnonceUpdated;
  data: {
    id: string;
    title: string;
    description: string;
    image: string;
    adress: string;
    phone: string;
    price: number;
    shippingFee: number;
    voteCount: number;
    userId: string;
    version: number;
    orderId?: string;
    voters?: string[];
  };
}
