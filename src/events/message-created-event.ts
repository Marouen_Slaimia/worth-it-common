import { Subjects } from './subjects';
import { MessageStatus } from './types/message-status';

export interface MessageCreatedEvent {
  subject: Subjects.MessageCreated;
  data: {
    id: string;
    version: number;
    userId: string;
    messageText: string;
    createdAt: string;
    status: MessageStatus;
    annonce: {
      id: string;
      title: string;
      userId: string;
    };
  };
}
